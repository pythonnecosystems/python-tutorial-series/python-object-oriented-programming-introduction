# Python 객체 지향 프로그래밍: 개요 <sup>[1](#footnote_1)</sup>

> <font size="3">Python의 객체 지향 프로그래밍(OOP) 기초를 알아본다.</font>

## 목차

1. [객체 지향(Obejct-Oriented) 프로그래밍이란?](./oop-intro.md#intro)
1. [Python의 클래스 및 객체](./oop-intro.md#sec_02)
1. [Python의 상속(Inheritance)과 다형성(Polymorphism)](./oop-intro.md#sec_03)
1. [Python의 캡슐화(Encapsulation)와 추상화(Abstarction)](./oop-intro.md#sec_04)
1. [Python의 매직 메서드(Magic Methids)와 연산자 오버로딩(Overloading)](./oop-intro.md#sec_05)
1. [요약](./oop-intro.md#summary)


<a name="footnote_1">1</a>: [Python Tutorial 18 — Python Exception Handling: Try, Except, Finally](https://python.plainenglish.io/python-tutorial-18-python-exception-handling-try-except-finally-ff21d6e589fc?sk=a6ad6b616e1ac17f36143b4f7343dccf)를 편역한 것이다.
