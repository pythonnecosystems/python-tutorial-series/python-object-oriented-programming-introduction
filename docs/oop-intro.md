# Python 객체 지향 프로그래밍: 개요

## <a name="intro"></a> 객체 지향(Obejct-Oriented) 프로그래밍이란?
객체 지향 프로그래밍(OOP)은 데이터와 동작을 객체라는 재사용 가능한 단위로 구성하는 프로그래밍 패러다임이다. 객체는 클래스의 인스턴스이며, 이는 객체의 속성과 메서드를 정의하는 템플릿이다. OOP를 통해 프로그래머는 쉽게 유지하고 확장할 수 있는 모듈과 확장 가능한 코드를 만들 수 있다.

이 포스팅에서는 Python에서 다음과 같은 객체 지향 프로그래밍의 기본을 살펴볼 것이다.

- Python에서 클래스 및 객체를 만드는 방법
- 상속 및 다형성을 사용하여 기존 코드를 재사용하고 수정하는 방법
- 구현 세부 정보를 숨기고 명확한 인터페이스를 제공하기 위해 캡슐화와 추상화를 사용하는 방법
- 매직 메소드 및 연산자 오버로딩를 사용하여 객체의 동작을 커스터마이징하는 방법

이 포스팅의 내용을 이해하면 OOP의 원리를 적용하여 자신만의 Python 프로그램을 만들 수 있을 것이다.

시작해요!

## <a name="sec_02"></a> Python의 클래스 및 객체
이 절에서는 Python으로 클래스와 객체를 만드는 방법을 알아본다. 클래스는 객체의 속성과 메소드를 정의하는 청사진이다. 속성은 객체의 데이터를 저장하는 변수이며, 메소드는 객체의 동작을 수행하는 함수이다.

Python에서 클래스를 만들려면 `class`키워드 다음에 클래스 이름과 콜론(`:`)을 사용한다. 클래스 이름은 [PEP 8 스타일 가이드](http://localhost:8888/files/development/python_projects/projects/content_bot/%5E1%5E?_xsrf=2%7C5f4d9628%7C92d534f31eaff1c20ab408bb144607f8%7C1704720102)를 따르고 **CamelCase** 표기법을 사용해야 한다. 예를 들어 `Person`이라는 이름의 클래스를 만들려면 다음과 같이 하여야 한다.

```python
# Define a class named Person
class Person:
    pass
```

`pass` 문은 클래스가 비어 있고 속성이나 메서드가 없음을 나타내는 데 사용된다. 들여쓰기를 사용하여 나중에 클래스에 속성과 메서드를 추가할 수 있다.

클래스의 객체를 만들려면 클래스 이름 뒤에 괄호를 사용한다. 변수에 객체를 할당하고 dot(`.`) 연산자를 사용하여 객체의 속성과 메서드를 액세스할 수 있다. 예를 들어 클래스 `Person`의 `bob`이라는 이름의 객체를 만들려면 다음과 같이 한다.

```python
# Create an object of the class Person
bob = Person()
```

```python
# Access the attributes and methods of the object
bob.name = "Bob" # Assign a value to the attribute name
bob.age = 25 # Assign a value to the attribute age
print(bob.name) # Print the value of the attribute name
print(bob.age) # Print the value of the attribute age
```

위 코드의 출력은 다음과 같다.

```
Bob
25
```

같은 클래스의 여러 객체를 생성하여 해당 객체의 속성에 다른 값을 할당할 수 있다. 예를 들어 `Person` 클래스의 `alice`라는 이름의 다른 객체를 생성하려면 다음과 같이 한다.

```python
# Create another object of the class Person
alice = Person()
```

```python
# Assign different values to the attributes of alice
alice.name = "Alice"
alice.age = 30

# Print the values of the attributes of alice
print(alice.name)
print(alice.age)
```

위 코드의 출력은 다음과 같다.

```
Alice
30
```

클래스의 메서드를 정의하려면 `def` 키워드 다음에 메서드 이름과 콜론(`:`)을 사용한다. 메서드의 이름은 [PEP 8 스타일 가이드](http://localhost:8888/files/development/python_projects/projects/content_bot/%5E1%5E?_xsrf=2%7C5f4d9628%7C92d534f31eaff1c20ab408bb144607f8%7C1704720102)를 따르고 **snake_case** 표기법을 사용해야 합니다. 메서드의 첫 번째 매개 변수는 `self`여야 하며 현재 객체를 참조한다. `self`를 사용하여 메서드 내에서 객체의 속성과 메서드에 액세스할 수 있다. 예를 들어 클래스 `Person`의 `greet`라는 이름의 메서드를 정의하려면 다음과 같이 작성한다.

```python
# Define a method named greet for the class Person
class Person:
    def greet(self): # The first parameter is self
        print("Hello, I am " + self.name + ".") # Use self to access the attribute name
```

객체의 메서드를 호출하려면 점(`.`) 연산자 다음에 메서드 이름과와 괄호를 사용한다. Python이 자동으로 수행하므로 `self`를 인수로 전달할 필요가 없다. 예를 들어 객체의 메서드를 `greet`를 호출하려면 다음과 같이 작성한다.

```python
# Call the method greet of the object bob
bob.greet() # No need to pass self as an argument
```

위 코드의 출력은 다음과 같다.

```
Hello, I am Bob.
```

다른 매개변수들을 `self` 다음에 열거하여 클래스의 메서드에 전달할 수 있다. 예를 들어 클래스 `Person`의 다른 객체를 인수로 사용하는 `intro`라는 메서드를 정의하려면 다음과 같이 작성한다.

```python
# Define a method named introduce that takes another object of the class Person as an argument
class Person:
    def introduce(self, other): # The second parameter is other
        print("Hello, I am " + self.name + ". This is " + other.name + ".") # Use self and other to access the attribute name
```

객체 `alice`를 인수로 사용하여 객체 `bob`의 `introduce`를 호출하려면 다음과 같이 작성한다.

```python
# Call the method introduce of the object bob with the object alice as an argument
bob.introduce(alice) # Pass alice as an argument
```

위 코드의 출력은 다음과 같다.

```
Hello, I am Bob. This is Alice.
```

이 절에서는 Python으로 클래스와 객체를 만드는 방법을 설명하였다. 객체의 속성과 메소드를 정의하고 접근하는 방법도 배웠다. [다음 절](#sec_03)에서는 상속과 다형성을 이용하여 기존 코드를 재사용하고 수정하는 방법을 설명할 것이다.

## <a name="sec_03"></a> Python의 상속(Inheritance)과 다형성(Polymorphism)
이 절에서는 Python에서 상속과 다형성을 사용하는 방법을 설명한다. 상속은 한 클래스가 다른 클래스의 속성과 메소드를 상속할 수 있도록 하는 메커니즘이다. 다형성은 어떤 대상이 맥락에 따라 다르게 행동하는 능력이다.

상속을 통해 부모 클래스의 특성을 상속받는 하위 클래스를 만들 수 있다. 이렇게 하면 기존 코드를 수정하지 않고 재사용하고 확장할 수 있다. 하위 클래스를 만들려면 클래스 키워드 다음에 하위 클래스 이름, 괄호, 부모 클래스 이름을 사용한다. 예를 들어, 클래스 `Person`으로부터 상속되는 `Student`라는 이름의 하위 클래스를 만들려면 다음과 같이 작성한다.

```python
# Define a subclass named Student that inherits from the class Person
class Student(Person):
    pass
```

기본적으로 하위 클래스는 상위 클래스의 모든 속성과 메서드를 상속받는다. 하위 클래스에 새 속성과 메서드를 추가하거나 기존 속성을 재정의할 수 있다. 예를 들어 하위 클래스에 `grade`라는 새 속성과 `study`라는 새 메서드를 추가하려면 다음과 같이 작성한다.

```python
# Add a new attribute and a new method to the subclass Student
class Student(Person):
    def __init__(self, name, age, grade): # Define a constructor method for the subclass
        super().__init__(name, age) # Call the constructor method of the parent class
        self.grade = grade # Assign a value to the new attribute grade
    def study(self): # Define a new method for the subclass
        print(self.name + " is studying hard for grade " + str(self.grade) + ".") # Use self to access the attributes name and grade
```

`__init__` 메서드는 객체를 생성할 때 호출하는 특수한 메서드이다. 객체의 속성을 초기화하는 데 사용된다. `super()` 함수는 상위 클래스의 메서드를 하위 클래스에서 호출하는 데 사용된다. 이때 하위 클래스의 `__init__` 메서드는 상위 클래스의 `__init__` 메서드를 호출하여 속성 이름과 나이를 초기화한다. 그런 다음 새 속성 `grade`에 값을 할당한다.

하위 클래스의 객체를 만들려면 상위 클래스의 객체를 만드는 것과 동일한 구문을 사용해야 한다. 또한 새 속성에 대한 값도 전달해야 한다. 예를 들어 하위 클래스 `Student`의 `tom`이라는 객체를 만들려면 다음과 같이 작성하여야 한다.

```python
# Create an object of the subclass Student
tom = Student("Tom", 18, 12)
```

```python
# Access the attributes and methods of the object
print(tom.name) # Print the value of the inherited attribute name
print(tom.age) # Print the value of the inherited attribute age
print(tom.grade) # Print the value of the new attribute grade
tom.greet() # Call the inherited method greet
tom.study() # Call the new method study
```

위 코드의 출력은 다음과 같다.

```
Tom
18
12
Hello, I am Tom.
Tom is studying hard for grade 12.
```

다형성에서는 다른 객체에 대해 동일한 메서드 이름을 사용할 수 있고 객체 타입에 따라 다른 결과를 얻을 수 있다. 예를 들어 클래스 `Person`의 `greet` 메소드는 간단한 인사말을 출력하는 반면, 하위 클래스 `Student`의 `greet` 메소드는 보다 구체적인 인사말을 출력하려고 한다. 다형성을 보여주기 위해 다양한 타입의 개체 리스트를 만들고 이를 통해 각 객체에 대해 동일한 메소드를 호출할 수 있다. 예를 들어 `bob`, `alice`와 `tom`이 포함된 `people`이라는 이름의 객체 리스트를 만들고 각 객체에 대해 `greet` 메소드를 호출하려면 다음과 같이 작성한다.

```python
# Add a new attribute and a new method to the subclass Student
class Student(Person):
    def __init__(self, name, age, grade): # Define a constructor method for the subclass
        super().__init__(name, age) # Call the constructor method of the parent class
        self.grade = grade # Assign a value to the new attribute grade
    def greet(self): # Define another method for the subclass
        print(self.name + "Hello I am " + str(self.name) + ". I am a student in grade " \
            + str(self.grade) + '.') # Use self to access the attributes name and grade
    def study(self): # Define a new method for the subclass
        print(self.name + " is studying hard for grade " + str(self.grade) + ".") # Use self to access the attributes name and grade
```

```python
# Create a list of objects of different types
people = [bob, alice, tom]

# Loop through the list and call the greet method for each object
for person in people:
    person.greet() # Call the same method name for different objects
```

위 코드의 출력은 다음과 같다.

```
Hello, I am Bob.
Hello, I am Alice.
Hello, I am Tom. I am a student in grade 12.
```

보다시피 `greet` 메서드는 객체의 타입에 따라 다르게 작동한다. 보다 유연하고 역동적인 코드를 작성할 수 있는 다형성의 한 예이다.

이 절에서는 Python에서 상속과 다형성을 사용하는 방법을 알아보았다. 부모 클래스로부터 상속받는 하위 클래스를 만드는 방법, 하위 클래스에 새로운 속성과 메서드를 덮어쓰고 추가하는 방법도 배웠다. [다음 절](#sec_04)에서는 캡슐화와 추상화를 사용하여 구현 세부 정보를 숨기고 명확한 인터페이스를 제공하는 방법을 설명할 것이다.

## <a name="sec_04"></a> Python의 캡슐화(Encapsulation)와 추상화(Abstarction)
이 절에서는 Python에서 캡슐화와 추상화를 사용하는 방법을 배울 것이다. 캡슐화는 객체의 구현 세부 사항을 숨기고 객체의 속성과 메서드에 대한 액세스를 제한할 수 있는 메커니즘이다. 추상화는 개체의 복잡성과 세부 사항을 숨기는 간단하고 명확한 인터페이스를 제공하는 과정이다.

캡슐화를 사용하면 객체의 데이터와 동작이 다른 객체나 코드에 의해 수정되거나 액세스되지 않도록 보호할 수 있다. Python에서 밑줄(`_`)을 사용하여 클래스의 속성이나 메서드에 대한 액세스 수준을 나타낼 수 있다. 액세스 수준에는 세 종류가 있다.

- **Public**: 속성 또는 메서드를 누구나 액세스할 수 있다. public 액세스를 표시하기 위하여는 밑줄이 사용되지 않는다. 예를 들어 클래스 `Person`의 속성 `name`과 메서드 `greet`은 public이다.
- **Protected**: 객체 자체 또는 객체의 하위 클래스만 액세스할 수 있다. 단일 밑줄(`_`)은 protected 액세스를 나타내는 데 사용된다. 예를 들어 각 객체에 대한 고유 식별자를 저장하는 클래스 `Person`의 `_id`라는 보호된 속성을 정의할 수 있다.
- **Private**: 속성 또는 메서드는 객체 자체 내에서만 액세스할 수 있다. 이중 밑줄(`__`)은 private 액세스를 나타내는 데 사용된다. 예를 들어 각 객체에 대해 임의 식별자를 생성하는 클래스 `Person`에 대해 `__generate_id`라는 이름의 private 메서드를 정의할 수 있다.

캡슐화를 시연하기 위해 클래스 `Person`을 수정하고 protected 속성 `_id`와 private 메서드 `__generate_id`를 추가할 수 있다. `__init__` 메서드를 수정하여 `_generate_id` 메서드를 사용하여 속성 `_id`에 값을 할당할 수도 있다. 예를 들어 다음과 같이 작성한다.

```python
# Import the random module to generate random numbers
import random


# Modify the class Person and add a protected attribute _id and a private method __generate_id
class Person:
    def __init__(self, name, age): # Modify the constructor method
        self.name = name # Assign a value to the public attribute name
        self.age = age # Assign a value to the public attribute age
        self._id = self.__generate_id() # Assign a value to the protected attribute _id using the private method __generate_id
    def greet(self): # Define a public method
        print("Hello, I am " + self.name + ".")
    def __generate_id(self): # Define a private method
        return random.randint(1000, 9999) # Return a random number between 1000 and 9999
```

클래스 `Person`의 객체를 만들려면 이전과 동일한 구문을 사용한다. 그러나 클래스 외부에서 보호되는 속성 `_id` 또는 private 메서드 `__generate_id`를 액세스할 수 없다. 예를 들어, 클래스 `Person`의 `bob`이라는 이름의 객체를 만들고 해당 속성과 메서드를 액세스하려고 하면 다음과 같이 작성할 수 있다.

```python
# Create an object of the class Person
bob = Person("Bob", 25)

# Access the public attributes and methods of the object
print(bob.name) # Print the value of the public attribute name
print(bob.age) # Print the value of the public attribute age
bob.greet() # Call the public method greet
# Try to access the protected attribute _id of the object
print(bob._id) # This will print the value of the protected attribute _id, but it is not recommended to do so
# Try to access the private method __generate_id of the object
print(bob.__generate_id()) # This will raise an AttributeError, as the private method __generate_id is not accessible from outside the class
```

위 코드의 출력은 다음과 같다.

```
Bob
25
Hello, I am Bob.
5678
AttributeError: 'Person' object has no attribute '__generate_id'
```

보다시피, protected 속성 `_id`는 클래스 외부에서 접근할 수 있지만 캡슐화의 원칙에 위배되므로 그렇게 하는 것을 권장하지 않는다. private 메소드 `_generate_id`를 더블 언더스코어에 의해 가려지기 때문에 클래스 외부에서 액세스할 수 없다. 이렇게 하면 객체의 데이터와 동작을 다른 객체나 코드에 의해 수정되거나 액세스되지 않도록 보호할 수 있다.

추상화를 사용하면 개체의 복잡성과 세부 사항을 숨긴 채 간단하고 명확한 인터페이스를 제공할 수 있다. Python에서 추상화 클래스와 메소드를 사용하여 구현을 제공하지 않고 클래스의 인터페이스를 정의할 수 있다. 추상화 클래스는 인스턴스화할 수 없지만 다른 클래스에서 상속할 수 있는 클래스이다. 추상화 메소드는 본체는 없지만 하위 클래스에 의해 재정의되어야 하는 메소드이다. Python에서 추상화 클래스나 메소드를 만들려면 `abc` 모듈을 가져와 `@abcmethod` 데코레이터(decorator)를 사용해야 한다. 예를 들어 `make_sound`라는 추상화 메소드를 가진 `Animal`이라는 추상화 클래스를 만들려면 다음과 같이 작성한다.

```python
# Import the abc module to create abstract classes and methods
from abc import ABC, abstractmethod

# Define an abstract class named Animal
class Animal(ABC):
    @abstractmethod # Use the @abstractmethod decorator to indicate an abstract method
    def make_sound(self): # Define an abstract method named make_sound
        pass # Use the pass statement to indicate an empty method
```

추상 클래스에서 상속되는 하위 클래스를 만들려면 추상 메서드를 재정의하고 구현을 제공해야 한다. 예를 들어 `Animal` 클래스로부터 상속된 메서드 `make_sound`를 재정의하는 `Dog`라는 이름의 하위 클래스를 만들려면 다음과 같이 작성한다.

```python
# Define a subclass named Dog that inherits from the abstract class Animal
class Dog(Animal):
    def make_sound(self): # Override the abstract method make_sound
        print("Woof!") # Provide the implementation for the method
```

서브클래스의 객체를 만들려면 일반 클래스의 객체를 만드는 것과 동일한 구문을 사용해야 한다. 그러나 추상 클래스의 객체는 인스턴스화되도록 의도된 것이 아니므로 생성할 수 없다. 예를 들어 서브클래스 `Dog`의 `rex`라는 객체를 만들고 메서드를 `make_sound`를 호출하려면 다음과 같이 작성해야 한다.

```python
# Create an object of the subclass Dog
rex = Dog()

# Call the method make_sound of the object
rex.make_sound() # This will print "Woof!"
# Try to create an object of the abstract class Animal
animal = Animal() # This will raise a TypeError, as the abstract class Animal cannot be instantiated
```

위 코드의 출력은 다음과 같다.

```
Woof!
TypeError: Can't instantiate abstract class Animal with abstract methods make_sound
```

보다시피 추상 클래스 `Animal`은 구현을 제공하지 않고도 그로부터 상속되는 하위 클래스에 대해 간단하고 명확한 인터페이스를 제공한다. 하위 클래스는 특정 기능에 따라 동일한 메서드 이름으로 다른 구현을 제공할 수 있다. 이렇게 하면 공통 인터페이스를 공유하지만 동작이 다른 클래스의 계층 구조를 만들 수 있다.

이 절에서는 Python에서 캡슐화와 추상화를 사용하는 방법을 살펴보았다. 또한 밑줄을 사용하여 속성이나 메서드에 대한 액세스 수준을 나타내는 방법, 추상화 클래스와 메서드를 사용하여 클래스의 인터페이스를 정의하는 방법도 배웠다. [다음 절](#sec_05)에서는 매직 메서드과 연산자 오버로딩을 사용하여 객체의 동작을 커스터마이징하는 방법을 살펴볼 예정이다.

## <a name="sec_05"></a> Python의 매직 메서드(Magic Methids)와 연산자 오버로딩(Overloading)
이 절에서는 Python에서 매직 메소드와 연산자 오버로딩을 사용하는 방법을 설명할 것이다. 매직 메소드는 이중 밑줄(`__`)로 시작하고 끝나는 특수 메소드로 객체에 대해 특정 연산을 수행하면 자동으로 호출된다. 연산자 오버로딩은 커스텀 객체에 대한 연산자(+, -, *, / 등)의 동작을 재정의하는 기능이다.

매직 메서드을 사용하면 객체의 동작을 커스터마이징하여 표현력과 직관력을 높일 수 있다. 예를 들어 `__init__` 메서드는 객체를 만들 때 부르는 매직 메서드이다. `__str__` 메서드는 `str()` 함수나 `print()` 문을 사용하여 객체를 문자열로 변환할 때 부르는 매직 메서드이다. `__add__` 메서드는 + 연산자를 사용하여 객체를 다른 객체에 추가할 때 부르는 매직 메서드이다. 객체의 동작을 커스터마이징할 때 사용할 수 있는 다른 매직 메서드들도 많다. 매직 메서드 목록과 그들에 대한 설명은 [여기서](https://www.analyticsvidhya.com/blog/2021/08/explore-the-magic-methods-in-python/) 찾을 수 있다.

매직 메서드를 설명하기 위해 클래스 `Person`을 수정하고 매직 메서드를을 추가할 수 있다. 예를 들어, 객체의 문자열 표현을 반환하는 `__str__` 메서드와 두 객체의 나이의 합을 반환하는 `__add__` 메서드를 추가할 수 있다. 예를 들어, 다음과 같이 작성할 수 있다.

```python
# Modify the class Person and add some magic methods
class Person:
    def __init__(self, name, age): # Define the constructor method
        self.name = name # Assign a value to the attribute name
        self.age = age # Assign a value to the attribute age
    def __str__(self): # Define the __str__ method
        return self.name + " (" + str(self.age) + ")" # Return a string representation of the object
    def __add__(self, other): # Define the __add__ method
        return self.age + other.age # Return the sum of the ages of two objects
```

클래스 `Person`의 객체를 만들려면 이전과 동일한 구문을 사용해야 한다. 그러나 이제 매직 메서드를 사용하여 객체에 대해 특정 작업을 수행할 수 있다. 예를 들어 클래스 `Person`의 `bob`과 `alice`라는 이름의 두 객체를 만들고 매직 메서드를 사용하려면 다음과 같이 학성할 수 있다.

```python
# Create two objects of the class Person
bob = Person("Bob", 25)
alice = Person("Alice", 30)

# Use the __str__ method to convert the objects to strings
print(str(bob)) # This will print "Bob (25)"
print(str(alice)) # This will print "Alice (30)"
# Use the __add__ method to add the objects using the + operator
print(bob + alice) # This will print 55, which is the sum of the ages of bob and alice
```

위 코드의 출력은 다음과 같다.

```
Bob (25)
Alice (30)
55
```

보다시피, 매직 메서드를 사용하면 객체의 동작을 사용자 정의할 수 있고 더 표현적이고 직관적으로 만들 수 있다. 문자열이나 숫자와 같은 내장형에 사용하는 것과 동일한 구문과 연산자를 사용할 수 있지만 객체 타입에 따라 다른 결과를 얻을 수 있다. 이것은 연산자 오버로딩의 한 예로, 커스텀 객체에 대한 연산자의 동작을 재정의할 수 있다.

이 절에서는 Python에서 매직 메서드와 연산자 오버로딩을 사용하는 방법을 배웠다. 또 객체의 동작을 커스터마이징하여 표현력과 직관력을 높이는 방법도 배웠다. [다음 절](#summary)에서는 포스팅을 마무리하고 추가 학습을 위해 몇 가지 추가 리소스를 제공한다.

## <a name="summary"></a> 요약
이 포스팅에서는 Python의 객체 지향 프로그래밍(OOP) 기초를 배웠다. 다음과 같은 방법을 설명하였다.

- Python에서 클래스 및 객체 생성
- 상속 및 다형성을 사용하여 기존 코드의 재사용 및 수정
- 캡슐화 및 추상화를 사용하여 구현 세부 정보를 숨기고 명확한 인터페이스 제공
- 매직 메서드와 연산자 오버로드를 사용하여 객체의 동작을 커스터마이징한다

OOP의 원리를 적용하여 쉽게 유지와 확장이 가능한 모듈형, 확장형 코드를 생성할 수 있다. 또한 내장 타입과 각각에서 상호 작용이 가능한 보다 표현력 있고 직관적인 객체를 생성할 수 있다.

객체 지향 프로그래밍은 복잡한 문제를 해결하고 우아한 솔루션을 만드는 데 도움을 줄 수 있는 강력하고 널리 사용되는 프로그래밍 패러다임이다. 그러나 이 포스팅은 Python에서 OOP의 기초만 다루었고, 배우고 탐구할 것이 훨씬 더 많다.
